FROM ruby:2.5.0

RUN apt-get update -qq && apt-get install -y \
  build-essential \
  libpq-dev \
  nodejs \
  xvfb \
  qt5-default \
  libqt5webkit5-dev \
  gstreamer1.0-plugins-base \
  gstreamer1.0-tools \
  gstreamer1.0-x

ENV APP_NAME airbnb-rental

ADD . /$APP_NAME/

ENV BUNDLE_JOBS 3
ENV BUNDLE_PATH /bundle
ENV GEM_PATH /bundle
ENV GEM_HOME /bundle
ENV BUNDLE_GEMFILE /$APP_NAME/Gemfile

WORKDIR /$APP_NAME
RUN gem install bundler
RUN bundle install

RUN chmod +x ./run-tests.sh

EXPOSE 3000

