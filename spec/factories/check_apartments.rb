FactoryBot.define do
  factory :check_apartment do
    address Faker::Address.city
    lat Faker::Address.latitude
    lng Faker::Address.longitude
    price Faker::Number.positive
  end
end
