require 'rails_helper'

RSpec.describe ComparePrice do
  describe "#perform" do
    let(:compare_price) {
      described_class.new(
        address: 'Kiev',
        lat: 5,
        lng: 5,
        price: Faker::Number.positive
      )
    }
    let(:income) { Faker::Number.positive }

    context "when income is" do
      it "positive" do
        stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
          status: 200,
          body: { data: { average_income_raw: compare_price.price + income } }.to_json,
          headers: {}
        )

        expect(compare_price.perform).to be_within(0.01).of(income.round(2))
      end

      it "negative" do
        stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
          status: 200,
          body: { data: { average_income_raw: compare_price.price - income } }.to_json,
          headers: {}
        )

        expect(compare_price.perform).to be_within(0.01).of(-income.round(2))
      end
    end

    context "when aribnb raise error" do
      it "income equal to -price" do
        stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
            status: [500, "Internal Server Error"]
        )

        expect(compare_price.perform).to be_within(0.01).of(-compare_price.price)
      end
    end
  end
end