require 'capybara/rspec'
require 'capybara/rails'

Capybara.configure do |config|
  config.default_driver = :webkit_billy
  config.javascript_driver = :webkit_billy
  config.server = :puma, { Silent: true }
end

Capybara::Webkit.configure do |config|
  config.allow_url('csi.gstatic.com')
  config.allow_url('maps.gstatic.com')
  config.allow_url('maps.google.com')
  config.allow_url('maps.googleapis.com')
end