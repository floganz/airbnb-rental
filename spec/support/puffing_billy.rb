require 'billy/capybara/rspec'

Billy.configure do |c|
  c.whitelist = ['localhost', '127.0.0.1']
  c.cache = true
  c.persist_cache = true
  c.cache_path = 'spec/request_cache/'
end