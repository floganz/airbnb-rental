module AutocompleteHelper
  def fill_google_autocomplete(id, value)
    sleep 1
    ele = find_by_id(id)
    if ele
      ele.set(value)
      sleep 1
      ele.send_keys(:down)
      ele.send_keys(:return)
    end
    sleep 1
  end
end