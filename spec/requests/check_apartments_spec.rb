require 'rails_helper'

RSpec.describe "CheckApartments", type: :request do
  describe "GET /check_apartments" do
    before(:each) do
      get check_apartments_path
    end

    it { expect(response).to be_success }
    it { expect(response.body).to include('Please fill form higher and press "Check"') }
  end

  describe "POST /check_apartments" do
    before(:each) do
      ActionController::Base.allow_forgery_protection = false
    end
    after(:each) do
      ActionController::Base.allow_forgery_protection = true
    end

    let(:valid_attributes) {
      attributes_for(:check_apartment)
    }
    let(:invalid_attributes) {
      {
        address: Faker::Address.city
      }
    }
    let(:income) { Faker::Number.positive }

    context "when params is" do
      context "valid" do
        it "returns possible income" do
          stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
            status: 200,
            body: {
              data: {
                average_income_raw: valid_attributes[:price] + income
              }
            }.to_json,
            headers: {}
          )

          post check_apartments_path, params: { check_apartment: valid_attributes }

          expect(response.body).to include("+#{income.round(2)}")
        end
      end

      context "not valid" do
        context "when price is" do
          it "not a number" do
            invalid_attributes[:price] = 'q'
            post check_apartments_path, params: { check_apartment: invalid_attributes }

            expect(response.body).to include("Price is not a number")
          end
        end
      end
    end
  end
end
