require 'rails_helper'

RSpec.describe "Check Apartment", type: :feature, js: true do
  context "User check apartments with" do
    before(:each) do
      visit "/check_apartments"
    end

    context "invalid params" do
      it "submit empty form" do
        click_button "Check"

        expect(page).to have_text("Address can't be blank")
        expect(page).to have_text("Price can't be blank")
        expect(page).to have_text("Price is not a number")
      end

      it "when address entered manually" do
        fill_in id: "check_apartment_address", with: "Kiev"
        fill_in id: "check_apartment_price", with: 500

        click_button "Check"

        expect(page).to have_text("Address can't be blank")
      end

      context "when price is" do
        it "blank" do
          fill_in id: "check_apartment_address", with: "Kiev"

          click_button "Check"

          expect(page).to have_text("Price can't be blank")
        end

        it "not a number" do
          fill_in id: "check_apartment_address", with: "Kiev"
          fill_in id: "check_apartment_price", with: 'q'

          click_button "Check"

          expect(page).to have_text("Price is not a number")
        end
      end
    end

    context "valid params" do
      context "when incpme is" do
        context "positive" do
          before(:each) do
            stub_request(:post, 'https://www.airbnb.com/wmpw_data')
                .to_return(
                  status: 200,
                  body: { data: { average_income_raw: 555.02 } }.to_json,
                  headers: {}
                )

            fill_google_autocomplete("check_apartment_address", "Kiev")
            fill_in id: "check_apartment_price", with: 100

            click_button "Check"
          end

          it "has right income" do
            expect(page).to have_text("+455.02")
          end

          it "has positive css class" do
            expect(page).to have_css('span.positive')
          end
        end

        context "negative" do
          before(:each) do
            stub_request(:post, 'https://www.airbnb.com/wmpw_data')
                .to_return(
                  status: 200,
                  body: { data: { average_income_raw: 555.02 } }.to_json,
                  headers: {}
                )

            fill_google_autocomplete("check_apartment_address", "Kiev")
            fill_in id: "check_apartment_price", with: 600

            click_button "Check"
          end

          it "has right income" do
            expect(page).to have_text("-44.98")
          end

          it "has negative css class" do
            expect(page).to have_css('span.negative')
          end
        end
      end
    end
  end
end