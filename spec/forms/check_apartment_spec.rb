require 'rails_helper'

RSpec.describe CheckApartment, type: :model do
  let(:check_apartment_empty) { described_class.new }
  let(:check_apartment_valid) {
    described_class.new(build(:check_apartment).instance_values)
  }

  describe "validation" do
    it { expect(check_apartment_empty).not_to be_valid }
    it { expect(check_apartment_valid).to be_valid }

    context "when price is" do
      it "negative" do
        check_apartment_valid.price = -1

        expect(check_apartment_valid).not_to be_valid
      end

      it "not a number" do
        check_apartment_valid.price = nil

        expect(check_apartment_valid).not_to be_valid
      end
    end

    context "when address is" do
      it "nil" do
        check_apartment_valid.address = nil

        expect(check_apartment_valid).not_to be_valid
      end
    end
  end

  describe "#check" do
    let(:income) { Faker::Number.positive }

    context "when form is" do
      context "valid" do
        context "when income is" do
          it "positive" do
            stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
              status: 200,
              body: { data: { average_income_raw: check_apartment_valid.price + income } }.to_json,
              headers: {}
            )

            check_apartment_valid.check

            expect(check_apartment_valid.possible_income).to be_within(0.01).of(income.round(2))
          end

          it "negative" do
            stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
              status: 200,
              body: { data: { average_income_raw: check_apartment_valid.price - income } }.to_json,
              headers: {}
            )

            check_apartment_valid.check

            expect(check_apartment_valid.possible_income).to be_within(0.01).of(-income.round(2))
          end
        end
      end

      context "not valid" do
        it "have nil positive income" do
          check_apartment_empty.price = Faker::Number.positive
          stub_request(:post, 'https://www.airbnb.com/wmpw_data').to_return(
            status: 200,
            body: { data: { average_income_raw: check_apartment_empty.price + income } }.to_json,
            headers: {}
          )

          check_apartment_valid.check

          expect(check_apartment_empty.possible_income).to be_nil
        end
      end
    end
  end
end