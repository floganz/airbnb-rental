require 'rails_helper'

RSpec.describe CheckApartmentsHelper, type: :helper do
  describe "#format_income" do
    context "when income is " do
      context "positive" do
        let(:income) { Faker::Number.positive.round(2) }

        it "return +income" do
          expect(helper.format_income(income)).to include("+#{income}")
        end

        it "return positive class" do
          expect(helper.format_income(income)).to include("class='positive'")
        end

        it "return right html code" do
          expect(helper.format_income(income)).to eq(
            "<span class='positive'>+#{income}</span>"
          )
        end
      end

      context "negative" do
        let(:income) { Faker::Number.negative.round(2) }

        it "return -income" do
          expect(helper.format_income(income)).to include(income.to_s)
        end

        it "return negative class" do
          expect(helper.format_income(income)).to include("class='negative'")
        end

        it "return right html code" do
          expect(helper.format_income(income)).to eq(
            "<span class='negative'>#{income}</span>"
          )
        end
      end

      context "nil" do
        let(:income) { nil }

        it "return right html code" do
          expect(helper.format_income(income)).to eq(
            '<span>Please fill form higher and press "Check"</span>'
          )
        end
      end
    end
  end
end
