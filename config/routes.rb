Rails.application.routes.draw do
  resource :check_apartments, only: %i[create show]

  root 'check_apartments#show'
end
