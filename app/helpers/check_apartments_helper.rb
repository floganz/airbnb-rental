module CheckApartmentsHelper
  def format_income(income)
    return '<span>Please fill form higher and press "Check"</span>' unless income
    if income.positive?
      income_str = '+' + income.to_s
      class_name = 'positive'
    else
      income_str = income.to_s
      class_name = 'negative'
    end
    "<span class='#{class_name}'>#{income_str}</span>"
  end
end
