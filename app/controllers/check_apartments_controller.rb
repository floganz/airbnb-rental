class CheckApartmentsController < ApplicationController

  def show; end

  def create
    check_apartment = ::CheckApartment.new(check_apartment_params).check

    render action: :show, locals: { check_apartment: check_apartment }
  end

  private

  def check_apartment_params
    params.require(:check_apartment).permit(:address, :price, :lat, :lng)
  end
end
