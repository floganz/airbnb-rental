class ComparePrice
  attr_reader :address, :price, :lat, :lng

  def initialize(address:, price:, lat:, lng:)
    @address = address
    @price = price.to_f
    @lat = lat
    @lng = lng
  end

  def perform
    (get_airbnb_price - price).round(2)
  end

  private

  def get_airbnb_price
    begin
      response = RestClient.post(
        'https://www.airbnb.com/wmpw_data',
        {
          room_type: 'entire_home_apt',
          duration: '1_month',
          person_capacity: 2,
          region: @address,
          lat: @lat,
          lng: @lng,
          multipart: true
        }
      )

      JSON.parse(response).dig('data', 'average_income_raw') || 0
    rescue => e
      puts e
      0
    end
  end
end