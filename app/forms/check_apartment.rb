class CheckApartment
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :address, :lat, :lng, :price, :possible_income

  validates :address, :price, presence: true, allow_blank: false
  validates :price, numericality: { greater_than_or_equal_to: 0 }

  def check
    if valid?
      @possible_income = ::ComparePrice.new(
        address: @address,
        lat: @lat,
        lng: @lng,
        price: @price
      ).perform
    end

    self
  end
end