# README

## Setup
* Create image with `docker build . -t=airbnb-rental:latest`
* Create DB using `docker-compose run web rails db:create`
* Run migrations `docker-compose run web rails db:migrate`

## Start server(port 3000)
`docker-compose up`

## Install missing gems
`docker-compose run web bundle`

## Run rails console
`docker-compose run web rails c`

## Run tests
Due to incompatibility of docker-compose and Xvfb (need to properly start webkit driver)
there is no option to just exec `docker-compose run web rspec`. 
As a solution was written this sh script that allow to run test without connecting to container and running command from it.

`docker-compose run web sh ./run-tests.sh rspec`
